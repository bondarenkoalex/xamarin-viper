﻿using System;
using UIKit;
using Viper.Common;

namespace Viper.iOS.Common
{
    public class Router : IRouter
    {
        public Router(UIViewController viewController)
        {
            this.ViewController = viewController;
        }

        protected UIViewController ViewController { get; set; }

        public virtual void Close()
        {
            this.ViewController.NavigationController.PopViewController(true);
        }
    }
}