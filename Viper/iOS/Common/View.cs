﻿using System;
using System.Collections.Generic;
using Foundation;
using GalaSoft.MvvmLight.Helpers;
using UIKit;
using Viper.Common;
using Viper.iOS.Common;

namespace Viper.iOS.Common
{
    [Register("View")]
    public class View<TViewModel> : UIViewController
        where TViewModel : IViewModel
    {
        private readonly List<Binding> bindings = new List<Binding>();

        public View(string name) : base(name, null)
        {
        }

        public TViewModel ViewModel { get; set; }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.ApplyBinding(this.bindings);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            this.ViewModel?.OnAppeared();
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
            this.ViewModel?.OnDisappeared();
        }

        protected virtual void ApplyBinding(List<Binding> bindings)
        {
        }
    }
}