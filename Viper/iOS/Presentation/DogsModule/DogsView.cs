﻿using System;
using System.Collections.Generic;
using Foundation;
using GalaSoft.MvvmLight.Helpers;
using GalaSoft.MvvmLight.Ioc;
using UIKit;
using Viper.Common;
using Viper.iOS.Common;
using Viper.Models;
using Viper.Presentation.DogsModule;
using Viper.Presentation.DogsModule.Implementation;

namespace Viper.iOS.Presentation.DogsModule
{
    public partial class DogsView : View<DogsViewModel>
    {
        private ObservableTableViewSource<Dog> source;

        [PreferredConstructor]
        public DogsView(IAssembly<DogsView> assembly) : base("DogsView")
        {
            assembly.Register(this);
        }

        protected override void ApplyBinding(List<Binding> bindings)
        {
            bindings.Add(this.SetBinding(
                () => this.ViewModel.Title,
                () => this.TitleLabel.Text));

            this.source = this.ViewModel.Dogs.GetTableViewSource(
                 CreateDogCell,
                 BindCellDelegate);

            this.source.SelectionChanged += this.SelectionChanged;

            this.TableView.Source = source;
        }

        private void SelectionChanged(object sender, EventArgs e)
        {
            var dog = this.source.SelectedItem;

            this.ViewModel.SelectDogCommand.Execute(dog);
        }

        private void BindCellDelegate(UITableViewCell cell, Dog dog, NSIndexPath path)
        {
            cell.TextLabel.Text = dog.Name;
            cell.DetailTextLabel.Text = dog.Owner;
        }

        private UITableViewCell CreateDogCell(NSString cellIdentifier)
        {
            return new UITableViewCell(UITableViewCellStyle.Default, null);
        }
    }
}