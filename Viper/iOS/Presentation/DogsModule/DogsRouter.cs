﻿using System;
using GalaSoft.MvvmLight.Ioc;
using UIKit;
using Viper.iOS.Common;
using Viper.iOS.Presentation.DogModule;
using Viper.Models;
using Viper.Presentation.DogsModule;

namespace Viper.iOS.Presentation.DogsModule
{
    public class DogsRouter : Router, IDogsRouter
    {
        public DogsRouter(UIViewController viewController) : base(viewController)
        {
        }

        public void ShowDog(Dog dog)
        {
            var dogView = SimpleIoc.Default.GetInstanceWithoutCaching<DogView>();
            dogView.ViewModel.Dog = dog;
            this.ViewController.NavigationController.PushViewController(dogView, true);
        }
    }
}
