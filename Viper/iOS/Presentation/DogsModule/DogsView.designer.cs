// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Viper.iOS.Presentation.DogsModule
{
    [Register("DogsView")]
    partial class DogsView
    {
        [Outlet]
        public UIKit.UITableView TableView { get; set; }

        [Outlet]
        public UIKit.UILabel TitleLabel { get; private set; }

        void ReleaseDesignerOutlets()
        {
            if (TitleLabel != null)
            {
                TitleLabel.Dispose();
                TitleLabel = null;
            }

            if (TableView != null)
            {
                TableView.Dispose();
                TableView = null;
            }
        }
    }
}
