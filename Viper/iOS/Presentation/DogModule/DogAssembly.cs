﻿using System;
using GalaSoft.MvvmLight.Ioc;
using Viper.Common;
using Viper.Presentation.DogModule;
using Viper.Presentation.DogModule.Implementation;

namespace Viper.iOS.Presentation.DogModule
{
    public class DogAssembly : IAssembly<DogView>
    {
        public void Register(DogView view)
        {
            var router = new DogRouter(view);
            var interactor = SimpleIoc.Default.GetInstance<IDogInteractor>();

            view.ViewModel = new DogViewModel(router, interactor);
        }
    }
}