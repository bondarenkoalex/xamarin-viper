﻿using System;
using System.Collections.Generic;
using GalaSoft.MvvmLight.Helpers;
using UIKit;
using Viper.Common;
using Viper.iOS.Common;
using Viper.Presentation.DogModule.Implementation;

namespace Viper.iOS.Presentation.DogModule
{
    public partial class DogView : View<DogViewModel>
    {
        public DogView(IAssembly<DogView> assembly) : base("DogView")
        {
            assembly.Register(this);
        }

        protected override void ApplyBinding(List<Binding> bindings)
        {
            bindings.Add(this.SetBinding(
                () => this.ViewModel.Dog.Name,
                () => this.TitleLabel.Text));

            bindings.Add(this.SetBinding(
                () => this.ViewModel.Dog.Owner,
                () => this.DetailsLabel.Text));

            this.DoButton.SetCommand(this.ViewModel.SomeCommand);
        }
    }
}