// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace Viper.iOS.Presentation.DogModule
{
    [Register("DogView")]
    partial class DogView
    {
        [Outlet]
        public UIKit.UILabel DetailsLabel { get; set; }

        [Outlet]
        public UIKit.UIButton DoButton { get; set; }

        [Outlet]
        public UIKit.UILabel TitleLabel { get; set; }

        void ReleaseDesignerOutlets()
        {
            if (TitleLabel != null)
            {
                TitleLabel.Dispose();
                TitleLabel = null;
            }

            if (DetailsLabel != null)
            {
                DetailsLabel.Dispose();
                DetailsLabel = null;
            }

            if (DoButton != null)
            {
                DoButton.Dispose();
                DoButton = null;
            }
        }
    }
}
