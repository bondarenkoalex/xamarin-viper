﻿using System;
using UIKit;
using Viper.iOS.Common;
using Viper.Presentation.DogModule;

namespace Viper.iOS.Presentation.DogModule
{
    public class DogRouter : Router, IDogRouter
    {
        public DogRouter(UIViewController viewController) : base(viewController)
        {
        }
    }
}