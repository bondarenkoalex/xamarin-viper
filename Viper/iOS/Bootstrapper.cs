﻿using System;
using GalaSoft.MvvmLight.Ioc;
using Viper.Common;
using Viper.iOS.Common;
using Viper.iOS.Presentation.DogModule;
using Viper.iOS.Presentation.DogsModule;

namespace Viper.iOS
{
    public static class Bootstrapper
    {
        public static void RegisterTypes()
        {
            SimpleIoc.Default.Register<IAssembly<DogsView>, DogsAssembly>();
            SimpleIoc.Default.Register<DogsView>();

            SimpleIoc.Default.Register<IAssembly<DogView>, DogAssembly>();
            SimpleIoc.Default.Register<DogView>();
        }
    }
}