﻿using Foundation;
using GalaSoft.MvvmLight.Ioc;
using UIKit;
using Viper.iOS.Presentation.DogsModule;

namespace Viper.iOS
{
    [Register("AppDelegate")]
    public class AppDelegate : UIApplicationDelegate
    {
        public override UIWindow Window { get; set; }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            Viper.Bootstrapper.RegisterTypes();
            Viper.iOS.Bootstrapper.RegisterTypes();

            this.Window = new UIWindow(UIScreen.MainScreen.Bounds);

            var startView = SimpleIoc.Default.GetInstance<DogsView>();
            var navigationController = new UINavigationController(startView);

            navigationController.SetNavigationBarHidden(true, false);

            this.Window.RootViewController = navigationController;
            this.Window.MakeKeyAndVisible();

            return true;
        }
    }
}