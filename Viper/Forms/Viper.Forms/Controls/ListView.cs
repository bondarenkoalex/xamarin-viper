﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace Viper.Forms.Controls
{
    public class ListView : Xamarin.Forms.ListView
    {
        public static readonly BindableProperty ItemClickedCommandProperty = BindableProperty.Create(
            nameof(ItemClickedCommand), 
            typeof(ICommand), 
            typeof(ListView), 
            null);
                                                                                     
        public ListView() : base()
        {
            this.ItemTapped += this.OnItemTapped;
        }

        public ICommand ItemClickedCommand
        {
            get
            {
                return (ICommand)this.GetValue(ItemClickedCommandProperty);
            }

            set
            {
                this.SetValue(ItemClickedCommandProperty, value);
            }
        }
        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item != null)
            {
                this.ItemClickedCommand?.Execute(e.Item);
            }
        }
    }
}