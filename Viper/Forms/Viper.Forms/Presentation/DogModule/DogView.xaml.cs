﻿using System;
using System.Collections.Generic;
using Viper.Common;
using Viper.Forms.Common;
using Viper.Presentation.DogModule.Implementation;
using Xamarin.Forms;

namespace Viper.Forms.Presentation.DogModule
{
    public partial class DogView : View<DogViewModel>
    {
        public DogView(IAssembly<DogView> assembly) : base()
        {
            this.InitializeComponent();

            assembly.Register(this);

            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}