﻿using System;
using Viper.Forms.Common;
using Viper.Presentation.DogModule;
using Xamarin.Forms;

namespace Viper.Forms.Presentation.DogModule
{
    public class DogRouter : Router, IDogRouter
    {
        public DogRouter(INavigation navigation) : base(navigation)
        {
        }
    }
}