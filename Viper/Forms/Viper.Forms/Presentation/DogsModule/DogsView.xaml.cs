﻿using System;
using System.Collections.Generic;
using Viper.Common;
using Viper.Forms.Common;
using Viper.Presentation.DogsModule.Implementation;
using Xamarin.Forms;

namespace Viper.Forms.Presentation.DogsModule
{
    public partial class DogsView : View<DogsViewModel>
    {
        public DogsView(IAssembly<DogsView> assembly) : base()
        {
            this.InitializeComponent();

            assembly.Register(this);

            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}