﻿using System;
using GalaSoft.MvvmLight.Ioc;
using Viper.Common;
using Viper.Presentation.DogsModule;
using Viper.Presentation.DogsModule.Implementation;

namespace Viper.Forms.Presentation.DogsModule
{
    public class DogsAssembly : IAssembly<DogsView>
    {
        public void Register(DogsView view)
        {
            var router = new DogsRouter(view.Navigation);
            var interactor = SimpleIoc.Default.GetInstance<IDogsInteractor>();

            view.ViewModel = new DogsViewModel(router, interactor);
            view.BindingContext = view.ViewModel;
        }
    }
}
