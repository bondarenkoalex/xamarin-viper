﻿using System;
using GalaSoft.MvvmLight.Ioc;
using Viper.Forms.Common;
using Viper.Forms.Presentation.DogModule;
using Viper.Models;
using Viper.Presentation.DogsModule;
using Xamarin.Forms;

namespace Viper.Forms.Presentation.DogsModule
{
    public class DogsRouter : Router, IDogsRouter
    {
        public DogsRouter(INavigation navigation) : base(navigation)
        {
        }

        public void ShowDog(Dog dog)
        {
            var dogView = SimpleIoc.Default.GetInstanceWithoutCaching<DogView>();
            dogView.ViewModel.Dog = dog;
            this.Navigation.PushAsync(dogView, true);
        }
    }
}