﻿using System;
using Viper.Common;
using Xamarin.Forms;

namespace Viper.Forms.Common
{
    public class View<TViewModel> : ContentPage
        where TViewModel : IViewModel
    {
        public View() : base()
        {
        }

        public TViewModel ViewModel { get; set; }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            this.ViewModel?.OnAppeared();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            this.ViewModel?.OnDisappeared();
        }
    }
}