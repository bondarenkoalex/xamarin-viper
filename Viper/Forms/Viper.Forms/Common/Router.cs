﻿using System;
using Viper.Common;
using Xamarin.Forms;

namespace Viper.Forms.Common
{
    public class Router : IRouter
    {
        public Router(INavigation navigation)
        {
            this.Navigation = navigation;
        }

        protected INavigation Navigation { get; set; }

        public virtual void Close()
        {
            this.Navigation.PopAsync(true);
        }
    }
}