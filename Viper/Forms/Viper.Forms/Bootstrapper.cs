﻿using System;
using GalaSoft.MvvmLight.Ioc;
using Viper.Common;
using Viper.Forms.Presentation.DogModule;
using Viper.Forms.Presentation.DogsModule;

namespace Viper.Forms
{
    public static class Bootstrapper
    {
        public static void RegisterTypes()
        {
            SimpleIoc.Default.Register<IAssembly<DogsView>, DogsAssembly>();
            SimpleIoc.Default.Register<DogsView>();

            SimpleIoc.Default.Register<IAssembly<DogView>, DogAssembly>();
            SimpleIoc.Default.Register<DogView>();
        }
    }
}
