﻿using GalaSoft.MvvmLight.Ioc;
using Viper.Forms.Presentation.DogsModule;
using Xamarin.Forms;

namespace Viper.Forms
{
    public partial class App : Application
    {
        public App()
        {
            this.InitializeComponent();

            Viper.Bootstrapper.RegisterTypes();
            Viper.Forms.Bootstrapper.RegisterTypes();

            this.MainPage = new NavigationPage(SimpleIoc.Default.GetInstance<DogsView>());
        }
    }
}