﻿using System;

namespace Viper.Models
{
    public class Dog
    {
        public string Name { get; set; }

        public string Owner { get; set; }
    }
}