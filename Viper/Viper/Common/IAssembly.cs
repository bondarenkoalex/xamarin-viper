﻿using System;
using Viper.Common;

namespace Viper.Common
{
    public interface IAssembly<TView>
    {
        void Register(TView view);
    }
}