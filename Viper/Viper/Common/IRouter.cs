﻿using System;

namespace Viper.Common
{
    public interface IRouter
    {
        void Close();
    }
}