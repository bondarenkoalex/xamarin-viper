﻿using System;
namespace Viper.Common
{
    public interface IViewModel
    {
        void OnAppeared();

        void OnDisappeared();
    }
}
