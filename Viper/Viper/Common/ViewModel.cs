﻿using System;
using GalaSoft.MvvmLight;

namespace Viper.Common
{
    public class ViewModel<TRouter, TInteractor> : ViewModelBase, IViewModel
        where TRouter : IRouter
        where TInteractor : IInteractor
    {
        public ViewModel(TRouter router, TInteractor interactor) : base()
        {
            this.Router = router;
            this.Interactor = interactor;
        }

        protected TRouter Router { get; private set; }

        protected TInteractor Interactor { get; private set; }

        public virtual void OnAppeared()
        {
        }

        public virtual void OnDisappeared()
        {
        }
    }
}