﻿using System;
using System.Collections.Generic;
using Viper.Models;

namespace Viper.Services.Implementation
{
    public class DogService : IDogService
    {
        public DogService ()
        {
        }

        public void DoSomething (Dog dog)
        {
        }

        public IEnumerable<Dog> GetDogs ()
        {
            return new List<Dog> () 
            {
                new Dog()
                {
                    Name  = "Test Name 1",
                    Owner = "Test Owner"
                },
                new Dog()
                {
                    Name  = "Test Name 2",
                    Owner = "Test Owner"
                },
                new Dog()
                {
                    Name  = "Test Name 3",
                    Owner = "Test Owner"
                },
            };
        }
    }
}