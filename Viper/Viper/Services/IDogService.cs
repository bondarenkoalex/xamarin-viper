﻿using System;
using System.Collections.Generic;
using Viper.Models;

namespace Viper.Services
{
    public interface IDogService
    {
        IEnumerable<Dog> GetDogs();

        void DoSomething (Dog dog);
    }
}