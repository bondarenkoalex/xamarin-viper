﻿using System;
using GalaSoft.MvvmLight.Ioc;
using Viper.Presentation.DogModule;
using Viper.Presentation.DogModule.Implementation;
using Viper.Presentation.DogsModule;
using Viper.Presentation.DogsModule.Implementation;
using Viper.Services;
using Viper.Services.Implementation;

namespace Viper
{
    public static class Bootstrapper
    {
        public static void RegisterTypes()
        {
            SimpleIoc.Default.Register<IDogService, DogService>();
            SimpleIoc.Default.Register<IDogsInteractor, DogsInteractor>();
            SimpleIoc.Default.Register<IDogInteractor, DogInteractor>();
        }
    }
}