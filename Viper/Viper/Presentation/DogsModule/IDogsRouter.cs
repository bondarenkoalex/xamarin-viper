﻿using System;
using Viper.Common;
using Viper.Models;

namespace Viper.Presentation.DogsModule
{
    public interface IDogsRouter : IRouter
    {
        void ShowDog(Dog dog);
    }
}