﻿using System;
using System.Collections.Generic;
using Viper.Common;
using Viper.Models;

namespace Viper.Presentation.DogsModule
{
    public interface IDogsInteractor : IInteractor
    {
        IEnumerable<Dog> GetDogs();
    }
}