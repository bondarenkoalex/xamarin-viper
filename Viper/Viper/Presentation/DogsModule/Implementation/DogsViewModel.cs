﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using PropertyChanged;
using Viper.Common;
using Viper.Models;

namespace Viper.Presentation.DogsModule.Implementation
{
    [ImplementPropertyChanged]
    public class DogsViewModel : ViewModel<IDogsRouter, IDogsInteractor>
    {
        public DogsViewModel(IDogsRouter router, IDogsInteractor interactor) : base(router, interactor)
        {
        }

        public ObservableCollection<Dog> Dogs { get; } = new ObservableCollection<Dog>();

        public string Title { get; private set; }

        public ICommand SelectDogCommand => new RelayCommand<Dog>(this.DoSelectDogCommand);

        public override void OnAppeared()
        {
            base.OnAppeared();

            this.Title = "Dogs";

            this.Dogs.Clear();

            var newDogs = this.Interactor.GetDogs();

            foreach (var dog in newDogs)
            {
                this.Dogs.Add(dog);
            }
        }

        private void DoSelectDogCommand(Dog dog)
        {
            this.Router.ShowDog(dog);
        }
    }
}