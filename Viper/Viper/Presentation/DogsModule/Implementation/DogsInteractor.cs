﻿using System;
using System.Collections.Generic;
using Viper.Models;
using Viper.Services;

namespace Viper.Presentation.DogsModule.Implementation
{
    public class DogsInteractor : IDogsInteractor
    {
        private readonly IDogService dogService;

        public DogsInteractor(IDogService dogService)
        {
            this.dogService = dogService;
        }

        public IEnumerable<Dog> GetDogs()
        {
            return this.dogService.GetDogs();
        }
    }
}