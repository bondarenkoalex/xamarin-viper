﻿using System;
using GalaSoft.MvvmLight.Command;
using PropertyChanged;
using Viper.Common;
using Viper.Models;

namespace Viper.Presentation.DogModule.Implementation
{
    [ImplementPropertyChanged]
    public class DogViewModel : ViewModel<IDogRouter, IDogInteractor>
    {
        public DogViewModel(IDogRouter router, IDogInteractor interactor) : base(router, interactor)
        {
        }

        public Dog Dog { get; set; }

        public RelayCommand SomeCommand => new RelayCommand(this.DoSomeCommand);

        private void DoSomeCommand()
        {
            this.Interactor.DoSomething(this.Dog);
            this.Router.Close();
        }
    }
}
