﻿using System;
using Viper.Models;
using Viper.Services;

namespace Viper.Presentation.DogModule.Implementation
{
    public class DogInteractor : IDogInteractor
    {
        private readonly IDogService dogService;

        public DogInteractor(IDogService dogService)
        {
            this.dogService = dogService;
        }

        public void DoSomething(Dog dog)
        {
            this.dogService.DoSomething(dog);
        }
    }
}