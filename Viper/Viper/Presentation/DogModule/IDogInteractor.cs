﻿using System;
using Viper.Common;
using Viper.Models;

namespace Viper.Presentation.DogModule
{
    public interface IDogInteractor : IInteractor
    {
        void DoSomething(Dog dog);
    }
}